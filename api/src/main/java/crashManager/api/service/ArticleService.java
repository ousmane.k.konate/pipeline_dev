package crashManager.api.service;

import crashManager.api.repository.ArticleRepository;
import org.springframework.stereotype.Service;

@Service
public class ArticleService {

    public final ArticleRepository articleRepository;

    public ArticleService(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }
}
