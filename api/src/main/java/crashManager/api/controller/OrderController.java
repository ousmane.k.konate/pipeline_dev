package crashManager.api.controller;

import crashManager.api.model.Orders;
import crashManager.api.repository.OrderRepository;
import crashManager.api.service.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

    private final OrderRepository orderRepository;

    private final OrderService orderService;

    public OrderController(OrderRepository orderRepository, OrderService orderService){
        this.orderRepository = orderRepository;
        this.orderService = orderService;
    }

    @GetMapping
    public List<Orders> findAll(){
        return orderRepository.findAll();
    }

    @GetMapping("/{id}")
    public Orders findById(@PathVariable("id") Long id){
        return orderRepository.findById(id).get();
    }
}
