package crashManager.api.controller;

import crashManager.api.model.Article;
import crashManager.api.repository.ArticleRepository;
import crashManager.api.service.ArticleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {

    private final ArticleRepository articleRepository;

    private final ArticleService articleService;

    public ArticleController(ArticleRepository articleRepository, ArticleService articleService){
        this.articleRepository = articleRepository;
        this.articleService = articleService;
    }

    @GetMapping
    public List<Article> findAll(){
        return articleRepository.findAll();
    }

    @GetMapping("/{id}")
    public Article findById(@PathVariable("id") Long id){
        return articleRepository.findById(id).get();
    }
}
