package crashManager.api.controller;

import crashManager.api.model.BankAccount;
import crashManager.api.repository.BankAccountRepository;
import crashManager.api.service.BankAccountService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/bankaccounts")
public class BankAccountController {

    private final BankAccountRepository bankAccountRepository;
    private final BankAccountService bankAccountService;

    public BankAccountController(BankAccountService bankAccountService, BankAccountRepository bankAccountRepository){
        this.bankAccountRepository = bankAccountRepository;
        this.bankAccountService = bankAccountService;
    }

    @GetMapping
    public List<BankAccount> findAll(){
        return bankAccountRepository.findAll();
    }

    @GetMapping("/{id}")
    public BankAccount findById(@PathVariable("id") Long id){
        return bankAccountRepository.findById(id).get();
    }
}
