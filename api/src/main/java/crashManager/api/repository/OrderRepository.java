package crashManager.api.repository;

import crashManager.api.model.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface OrderRepository extends JpaRepository<Orders, Long> {
}
