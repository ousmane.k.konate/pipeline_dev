package crashManager.api.repository;

import crashManager.api.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface ArticleRepository extends JpaRepository<Article, Long> {
}
