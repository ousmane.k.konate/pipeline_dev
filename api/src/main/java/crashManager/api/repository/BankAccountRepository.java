package crashManager.api.repository;

import crashManager.api.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface BankAccountRepository extends JpaRepository<BankAccount, Long> {
}
