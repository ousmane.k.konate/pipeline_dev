package crashManager.api.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Article {

    @Id @GeneratedValue
    @Column(name = "id")
    private Long id;
    private String title;
    private String description;
    private String image;
    private Integer price;
    private Integer quantity;

    public Article(){}

    public Article(String title, String description, String image, Integer price, Integer quantity) {
        this.title = title;
        this.description = description;
        this.image = image;
        this.price = price;
        this.quantity = quantity;
    }
}
