package crashManager.api.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class BankAccount {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    private Integer amount;
    public BankAccount(){}
    public BankAccount(Integer amount) {
        this.amount = amount;
    }
}
