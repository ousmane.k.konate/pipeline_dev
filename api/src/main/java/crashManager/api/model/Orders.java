package crashManager.api.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table
public class Orders {

    @Id @GeneratedValue
    @Column(name = "id")
    private Long id;
    private Integer price;

    public Orders() {}
    public Orders(Integer price){
        this.price = price;
    }
}
