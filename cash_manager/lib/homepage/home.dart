import 'package:flutter/material.dart';
import 'package:cash_manager/login/login.dart';
import 'package:cash_manager/homepage/learnmac.dart';
import 'package:cash_manager/homepage/learniphone.dart';
import 'package:cash_manager/homepage/learnwatch.dart';
import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:cash_manager/nfc/scan.dart';
import 'package:get/get.dart';
import 'package:nfc_manager/platform_tags.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _Home();
}

class _Home extends State<Home> {
  int currentPageIndex = 0;
  final textcontroller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: AnimSearchBar(
          width: 400,
          textController: textcontroller,
          color: Color.fromARGB(255, 158, 154, 154),
          suffixIcon: Icon(
            Icons.search,
          ),
          onSuffixTap: () {
            setState(() {
              textcontroller.clear();
            });
          },
        ),
        backgroundColor: Color.fromARGB(255, 14, 14, 222),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.shopping_cart),
            onPressed: () {
              Get.to(Scan());
            },
          ),
        ],
      ),
      bottomNavigationBar: NavigationBar(
        onDestinationSelected: (int index) {
          setState(() {
            currentPageIndex = index;
          });
        },
        selectedIndex: currentPageIndex,
        destinations: const <Widget>[
          NavigationDestination(
            icon: Icon(Icons.home_outlined),
            label: 'Home',
          ),
          NavigationDestination(
            icon: Icon(Icons.search),
            label: 'Search',
          ),
          NavigationDestination(
            icon: Icon(Icons.person),
            label: 'Profil',
          ),
        ],
      ),
      body: <Widget>[
        SingleChildScrollView(
          // PAGE HOME
          child: Column(
            children: <Widget>[
              Card(
                color: Color.fromARGB(255, 158, 154, 154),
                elevation: 4.0,
                child: Column(
                  children: [
                    ListTile(
                      title: Text('MacBook Pro 13"'),
                      subtitle: Text('Don’t take it lightly.'),
                      trailing: IconButton(
                        icon: Icon(
                          Icons.add_circle_outline,
                          size: 40,
                        ),
                        onPressed: () {},
                      ),
                    ),
                    Container(
                      height: 200.0,
                      child: Image.asset(
                        'assets/macbook.png',
                        height: 100,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      alignment: Alignment.center,
                      child: Text('From \$1299'),
                    ),
                    ButtonBar(
                      children: [
                        TextButton(
                          child: const Text(
                            'LEARN MORE',
                            style: TextStyle(
                              color: Color.fromARGB(255, 19, 12, 10),
                            ),
                          ),
                          onPressed: () {
                            Get.to(Learnmac());
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
              Card(
                color: Color.fromARGB(255, 158, 154, 154),
                elevation: 4.0,
                child: Column(
                  children: [
                    ListTile(
                      title: Text('Applce Watch Ultra'),
                      subtitle: Text('Adventure awaits.'),
                      trailing: IconButton(
                        icon: Icon(
                          Icons.add_circle_outline,
                          size: 40,
                        ),
                        onPressed: () {},
                      ),
                    ),
                    Container(
                      height: 200.0,
                      child: Image.asset(
                        'assets/applewatch.png',
                        height: 100,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      alignment: Alignment.center,
                      child: Text('From \$799'),
                    ),
                    ButtonBar(
                      children: [
                        TextButton(
                          child: const Text(
                            'LEARN MORE',
                            style: TextStyle(
                              color: Color.fromARGB(255, 19, 12, 10),
                            ),
                          ),
                          onPressed: () {
                            Get.to(Learnwatch());
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
              Card(
                color: Color.fromARGB(255, 158, 154, 154),
                elevation: 4.0,
                child: Column(
                  children: [
                    ListTile(
                      title: Text('Iphone 14 Pro'),
                      subtitle: Text('The ultimate iPhone.'),
                      trailing: IconButton(
                        icon: Icon(
                          Icons.add_circle_outline,
                          size: 40,
                        ),
                        onPressed: () {},
                      ),
                    ),
                    Container(
                      height: 200.0,
                      child: Image.asset(
                        'assets/iphone.png',
                        height: 100,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      alignment: Alignment.center,
                      child: Text('From \$999'),
                    ),
                    ButtonBar(
                      children: [
                        TextButton(
                          child: const Text(
                            'LEARN MORE',
                            style: TextStyle(
                              color: Color.fromARGB(255, 19, 12, 10),
                            ),
                          ),
                          onPressed: () {
                            Get.to(Learniphone());
                          },
                        )
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),

        // PAGE SEARCH
        Container(
          alignment: Alignment.center,
          child: const Text('Page 2'),
        ),

        // PAGE PROFIL
        Container(
          color: Color.fromARGB(255, 255, 255, 255),
          alignment: Alignment.center,
          child: ListView(
            padding: const EdgeInsets.all(16),
            children: <Widget>[
              Image.asset(
                'assets/profilimg.png',
                height: 100,
              ),
              const Center(
                child: Text(
                  "Bonjour Quentin",
                  style: TextStyle(
                      color: Color.fromARGB(255, 19, 12, 10), fontSize: 20),
                ),
              ),
              const SizedBox(height: 40),
              Text("Email", textAlign: TextAlign.center),
              TextFormField(
                style: const TextStyle(fontSize: 20.0, color: Colors.white),
                decoration: InputDecoration(
                  hintStyle:
                      const TextStyle(fontSize: 20.0, color: Colors.white),
                  fillColor: Colors.black.withOpacity(0.2),
                  filled: true,
                  labelStyle:
                      const TextStyle(color: Colors.white, fontSize: 16.0),
                  border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8.0),
                    ),
                    borderSide: BorderSide(
                      color: Colors.orange,
                      width: 1.0,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 8),
              Text("Password", textAlign: TextAlign.center),
              TextFormField(
                obscureText: true,
                style: const TextStyle(fontSize: 15.0, color: Colors.white),
                decoration: InputDecoration(
                  hintStyle:
                      const TextStyle(fontSize: 15.0, color: Colors.white),
                  fillColor: Colors.black.withOpacity(0.2),
                  filled: true,
                  labelStyle:
                      const TextStyle(color: Colors.white, fontSize: 16.0),
                  border: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(8.0),
                    ),
                    borderSide: BorderSide(
                      color: Colors.green,
                      width: 1.0,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 200),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: const Size.fromHeight(50),
                  backgroundColor: Color.fromARGB(255, 254, 1, 1),
                ),
                child: const Text(
                  'Logout',
                  style: TextStyle(fontSize: 24),
                ),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Login()),
                  );
                },
              ),
            ],
          ),
        ),
      ][currentPageIndex],
    );
  }
}
