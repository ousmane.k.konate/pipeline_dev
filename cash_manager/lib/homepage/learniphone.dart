import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Learniphone extends StatefulWidget {
  const Learniphone({super.key});

  @override
  State<Learniphone> createState() => _Learniphone();
}

class _Learniphone extends State<Learniphone> {
  int currentPageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        title: const Text('Cash Manager'),
        backgroundColor: Color.fromARGB(255, 14, 14, 222),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.shopping_cart),
            onPressed: () {},
          ),
        ],
      ),
      body: <Widget>[
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                height: 200.0,
                child: Image.asset(
                  'assets/iphone.png',
                  height: 100,
                ),
              ),
              Card(
                color: Color.fromARGB(255, 158, 154, 154),
                elevation: 4.0,
                child: Column(
                  children: [
                    ListTile(
                      title: Text('iPhone 14 Pro'),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      alignment: Alignment.bottomLeft,
                      child: Text(
                          'A magical new way to interact with iPhone. Groundbreaking safety features designed to save lives. An innovative 48MP camera for mind-blowing detail. All powered by the ultimate smartphone chip.\n\nDesigned for durability.\nWith Ceramic Shield, tougher than any smartphone glass.\nWater resistance.\nSurgical-grade stainless steel.\n6.1″ and 6.7″ display sizes.\nAll in four Pro colors.'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ][currentPageIndex],
    );
  }
}
