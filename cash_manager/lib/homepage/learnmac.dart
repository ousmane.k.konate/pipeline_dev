import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Learnmac extends StatefulWidget {
  const Learnmac({super.key});

  @override
  State<Learnmac> createState() => _Learnmac();
}

class _Learnmac extends State<Learnmac> {
  int currentPageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        title: const Text('Cash Manager'),
        backgroundColor: Color.fromARGB(255, 14, 14, 222),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.shopping_cart),
            onPressed: () {},
          ),
        ],
      ),
      body: <Widget>[
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                height: 200.0,
                child: Image.asset(
                  'assets/macbook.png',
                  height: 100,
                ),
              ),
              Card(
                color: Color.fromARGB(255, 158, 154, 154),
                elevation: 4.0,
                child: Column(
                  children: [
                    ListTile(
                      title: Text('MacBook Pro 13"'),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      alignment: Alignment.bottomLeft,
                      child: Text(
                          'Power. It’s in the Air.\nMacBook Air with M1 is an incredibly portable laptop it’s nimble and quick, with a silent, fanless design and a beautiful Retina display. \nThanks to its slim profile and all‑day battery life, this Air moves at the speed of lightness.\n \nM1 is our first chip designed specifically for Mac. Apple silicon integrates the CPU, GPU, Neural Engine, I/O, and so much more onto a single tiny chip.\nPacked with an astonishing 16 billion transistors, M1 delivers exceptional performance, custom technologies, and unbelievable power efficiency — a major breakthrough for Mac.'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ][currentPageIndex],
    );
  }
}
