import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Learnwatch extends StatefulWidget {
  const Learnwatch({super.key});

  @override
  State<Learnwatch> createState() => _Learnwatch();
}

class _Learnwatch extends State<Learnwatch> {
  int currentPageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        title: const Text('Cash Manager'),
        backgroundColor: Color.fromARGB(255, 14, 14, 222),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.shopping_cart),
            onPressed: () {},
          ),
        ],
      ),
      body: <Widget>[
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                height: 200.0,
                child: Image.asset(
                  'assets/applewatch.png',
                  height: 100,
                ),
              ),
              Card(
                color: Color.fromARGB(255, 158, 154, 154),
                elevation: 4.0,
                child: Column(
                  children: [
                    ListTile(
                      title: Text('Apple watch Ultra'),
                    ),
                    Container(
                      padding: EdgeInsets.all(16.0),
                      alignment: Alignment.bottomLeft,
                      child: Text(
                          'To build the ultimate sports watch, we crafted every element with painstaking attention to detail for unparalleled performance. \nTitanium strikes the perfect balance between weight, ruggedness, and corrosion resistance. The new case design rises up to surround the flat sapphire crystal and protect it from edge impacts. The Digital Crown is larger and the side button is raised from the case, making them easier to use while you’re wearing gloves.\n \nReady for Action. The new Action button gives you quick, physical control over a variety of functions. It’s customizable and can do things like control a workout, mark a Compass Waypoint, or begin a dive. Just like you, it’s full of potential.'),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ][currentPageIndex],
    );
  }
}
