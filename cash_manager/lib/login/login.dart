import 'package:flutter/material.dart';
import 'package:cash_manager/login/form.dart';
import 'package:cash_manager/homepage/home.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: const Text(
          'Cash Manager',
        ),
        backgroundColor: Color.fromARGB(255, 14, 14, 222),
      ),
      body: ListView(
        padding: const EdgeInsets.all(16),
        children: <Widget>[
          const Center(
            child: Text(
              "Login",
              style: TextStyle(
                  color: Color.fromARGB(255, 19, 12, 10), fontSize: 50),
            ),
          ),
          const SizedBox(height: 125),
          Text("Email"),
          TextFormField(
            style: const TextStyle(fontSize: 20.0, color: Colors.white),
            decoration: InputDecoration(
              hintStyle: const TextStyle(fontSize: 20.0, color: Colors.white),
              fillColor: Colors.black.withOpacity(0.2),
              filled: true,
              labelStyle: const TextStyle(color: Colors.white, fontSize: 16.0),
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(8.0),
                ),
                borderSide: BorderSide(
                  color: Colors.orange,
                  width: 1.0,
                ),
              ),
            ),
            controller: nameController,
          ),
          const SizedBox(height: 8),
          Text("Password"),
          TextFormField(
            obscureText: true,
            style: const TextStyle(fontSize: 20.0, color: Colors.white),
            decoration: InputDecoration(
              hintStyle: const TextStyle(fontSize: 20.0, color: Colors.white),
              fillColor: Colors.black.withOpacity(0.2),
              filled: true,
              labelStyle: const TextStyle(color: Colors.white, fontSize: 16.0),
              border: const OutlineInputBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(8.0),
                ),
                borderSide: BorderSide(
                  color: Colors.green,
                  width: 1.0,
                ),
              ),
            ),
            controller: passwordController,
          ),
          const SizedBox(height: 40),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              minimumSize: const Size.fromHeight(50),
              backgroundColor: Color.fromARGB(255, 14, 14, 222),
            ),
            child: const Text(
              'Login',
              style: TextStyle(fontSize: 24),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Home()),
              );
            },
          ),
          const SizedBox(height: 10),
          TextButton(
            child: const Text(
              "New User? Create Account",
              style: TextStyle(color: Color.fromARGB(255, 19, 12, 10)),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Formu()),
              );
            },
          ),
        ],
      ),
    );
  }
}
